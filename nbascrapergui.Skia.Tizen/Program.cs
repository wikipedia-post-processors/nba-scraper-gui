using Tizen.Applications;
using Uno.UI.Runtime.Skia;

namespace nbascrapergui.Skia.Tizen
{
	class Program
{
	static void Main(string[] args)
	{
		var host = new TizenHost(() => new nbascrapergui.App(), args);
		host.Run();
	}
}
}
